<?php
declare(strict_types=1);

namespace Rabbit\Core;

interface MessageBrokerInterface
{
    /**
     * @param string $queueName
     * @return bool
     */
    public function createQueue(string $queueName): bool;

    /**
     * @param string $message
     * @return bool
     */
    public function sendMessage(string $message): bool;

    /**
     * @return bool
     */
    public function getMessage(): bool;
}