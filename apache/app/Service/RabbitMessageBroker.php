<?php
declare(strict_types=1);

namespace Rabbit\Service;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Rabbit\Core\MessageBrokerInterface;

class RabbitMessageBroker implements MessageBrokerInterface
{
    /** @var AMQPStreamConnection */
    private $connection;

    /** @var \PhpAmqpLib\Channel\AMQPChannel */
    private $channel;

    private $queue;

    /**
     * RabbitMessageBrokerInterface constructor.
     * @param string $host
     */
    public function __construct(string $host)
    {
        try {
            $this->connection = new AMQPStreamConnection($host, 5672, 'guest', 'guest');
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @param string $queueName
     * @return bool
     */
    public function createQueue(string $queueName): bool
    {
        try {
            $this->channel = $this->connection->channel();
            $this->channel->queue_declare($queueName, false, false, false, false);
            $this->queue = $queueName;
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @param string $message
     * @return bool
     */
    public function sendMessage(string $message): bool
    {
        try {
            $message = new AMQPMessage('Hello World!');
            $this->channel->basic_publish($message, '', $this->queue);
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function getMessage(): bool
    {
        // TODO: Implement getMessage() method.
    }

    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }
}
