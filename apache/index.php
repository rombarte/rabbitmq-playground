<?php

require_once "vendor/autoload.php";

try {
    $messageBroker = new \Rabbit\Service\RabbitMessageBroker('message_broker');
    $messageBroker->createQueue('createDataReport');
    $messageBroker->sendMessage('stage1');
} catch (\Exception $exception) {
    echo sprintf("Fatal error: %s", $exception->getMessage());
}
